/**
 * SQS client represents a wrapper for AWS SQS SDK. It provides pub-sub and req-res patterns out-of-the-box
 */
declare class SQS {

    /**
     * @param {SqsOptions} options
     */
    constructor(options: SqsOptions);

    /**
     * Initialize service with response queue. Need to invoke if you are going to use req-res pattern (request() and listen() methods). Response queue will be created dynamically.
     * Dead queue should be already exist
     * @param {Object} options
     * @param {string} options.responseSubject - name of queue where responses will be sent
     * @param {Object} options.redrivePolicy - configuration of dead queue for "response" queue
     * @param {doneCallback} callback - will be invoked when initialization if done
     */
    initializeWithResponseQueue(options: {responseSubject: string, redrivePolicy: {deadLetterTargetArn: string, maxReceiveCount: number}}, callback: doneCallback): void;

    /**
     * Publish message to queue with name of "subject"
     * @param {string} subject - name of queue where to send
     * @param {SqsOptions} options - overrides default configuration of SQS clien
     * @param {Data} data - data which need to send
     */
    publish(subject: string, options: Options, data: Data): void;

    /**
     * Start listen to new messages sent to queue with name of "subject"
     * @param {string} subject - name of queue to listen
     * @param {SqsOptions} options - overrides default configuration of SQS service
     * @param {handlerCallback} handler - will be invoked when new message received from SQS server
     */
    process(subject: string, options: Options, handler: handlerCallback): void;

    /**
     * Send request to listener of queue with name of "subject" and wait for a response
     * @param {string} subject - name of queue where to send
     * @param {SqsOptions} options - overrides default configuration of SQS service
     * @param {Data} data - data which need to send with a request
     * @param {responseHandlerCallback} callback - will be invoked when an answer received (via "responseSubject" queue of this client)
     */
    request(subject: string, options: Options, data: Data, callback: responseHandlerCallback): void;

    /**
     * Listen to new messages sent to queue with name of "subject", process it and send answer back
     * @param {string} subject - name of queue to listen
     * @param {SqsOptions} options - overrides default configuration of SQS service
     * @param {listenerHandlerCallback} callback - will be invoked when new message received from SQS server
     */
    listen(subject: string, options: Options, callback: listenerHandlerCallback): void;

    /**
     * Create new queue by name
     * @param {string} queueName
     * @param options - overrides default configuration of SQS service
     * @param {createQueueCallback} callback - will be invoked when new queue created
     */
    createQueue(queueName: string, options: any, callback: createQueueCallback): void;

    /**
     * Delete a queue by name
     * @param {string} queueName
     * @param options - overrides default configuration of SQS service
     * @param {deleteQueueCallback} callback - will be invoked when a queue deleted
     */
    deleteQueue(queueName: string, options: any, callback: deleteQueueCallback): void;

    /**
     * Stop message broker. Deletes response queue and stops process queues
     */
    stop(): void;
}

interface SqsOptions {
    /**
     * AWS access key id
     */
    accessKeyId?: string;
    /**
     * AWS secret access key
     */
    secretAccessKey?: string;
    /**
     * AWS region
     */
    region: string;
    /**
     * environment (development, stage, production)
     */
    env: string;
    /**
     * queue prefix (like v1, v2, krs, kyc). empty if not needed
     */
    queuePrefix: string;
    fifo?: any;
    sslEnabled: boolean;
    deleteMessagesInterval: number;
    sendMessagesInterval: number;
    numberOfRetrieveMessages: number;
    visibilityTimeout: number;
    retrieveDuration: number;
    requestTimeout: number;
}

interface Options {
    /**
     * AWS access key id
     */
    accessKeyId?: string;
    /**
     * AWS secret access key
     */
    secretAccessKey?: string;
    /**
     * AWS region
     */
    region?: string;
    /**
     * environment (development, stage, production)
     */
    env?: string;
    /**
     * queue prefix (like v1, v2, krs, kyc). empty if not needed
     */
    queuePrefix?: string;
    fifo?: any;
    sslEnabled?: boolean;
    deleteMessagesInterval?: number;
    sendMessagesInterval?: number;
    numberOfRetrieveMessages?: number;
    visibilityTimeout?: number;
    retrieveDuration?: number;
    requestTimeout?: number;
}

interface Data {
    payload?: any,
    meta?: any
}

type handlerCallback = (data: Data, done: doneCallback) => void;

type doneCallback = (error?: Error) => void;

type createQueueCallback = (error?: Error, data?: any) => void;

type deleteQueueCallback = (error?: Error, data?: any) => void;

type responseHandlerCallback = (data: Data) => void;

type listenerHandlerCallback = (data: Data, callback: listenerDoneCallback) => void;

type listenerDoneCallback = (error?: Error, responseData?: Data) => void;


export = SQS;

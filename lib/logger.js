let createLogger = require('devorchestra-logger');
let packageJson = require('../package.json');

let logger = createLogger(packageJson.name, process.env.SQS_LOG_LEVEL);
module.exports = logger;
let AWS = require('aws-sdk');
let _ = require('lodash');
let generateUuid = require('uuid/v4');
let async = require('async');
const EventEmitter = require('events');

let Queue = require('./queue');
let logger = require('./logger');
let {validateOptions, buildFullQueueName} = require(`./utils`);


const DEFAULT_OPTIONS = {
    apiVersion: '2012-11-05',
    env: 'development',
    numberOfRetrieveMessages: 10,
    retrieveDuration: 20,
    visibilityTimeout: 30, // sec
    messageAttributeNames: ['All'],
    numberOfListeners: 1,
    deleteMessagesInterval: 150,
    sendMessagesInterval: 150,
    sslEnabled: true,
    messageSizeLimit: 250000,
    requestTimeout: 30000
};

class SQS {

    constructor(options) {

        // options
        validateOptions(options);
        this.options = {};
        _.merge(this.options, DEFAULT_OPTIONS, options);

        // configure aws sdk
        AWS.config.update({region: options.region});
        this.awsSQS = new AWS.SQS({
            apiVersion: this.options.apiVersion,
            accessKeyId: this.options.accessKeyId,
            secretAccessKey: this.options.secretAccessKey,
            sslEnabled: this.options.sslEnabled
        });

        this.processQueues = new Map();
        this.publishQueues = new Map();

        this.internalMessageBus = new EventEmitter();
    }

    initializeWithResponseQueue({responseSubject, redrivePolicy}, callback) {
        this.responseQueueName = responseSubject + '-' + generateUuid();
        this.createQueue(buildFullQueueName(this.responseQueueName, this.options), redrivePolicy, (error) => {
            if (error) {
                return callback(error);
            }
            // TODO Be aware, if queue was just created it might not be seen during deletion process
            this._addPOSIXSignalsListeners();
            this.process(this.responseQueueName, {}, (data, done) => {
                let requestId = data.meta.requestId;
                this.internalMessageBus.emit(requestId, data);
                done();
            });

            callback();
        });

    }

    createQueue(queueName, redrivePolicy, callback) {
        async.waterfall([

            (queueChecked) => {
                this.getQueues(queueName, (error, list) => {
                    if (error) return queueChecked(error);
                    if (list) {
                        let newError = new Error(`Queue ${queueName} is already exist`);
                        newError.status = 409;
                        return queueChecked(newError);
                    }
                    queueChecked(null);
                })
            },

            (queueCreated) => {
                let params = {QueueName: queueName, Attributes: {}};
                if (queueName.slice(queueName.length - 5) === '.fifo') {
                    params.Attributes.FifoQueue = 'true';
                }
                if (redrivePolicy) {
                    params.Attributes.RedrivePolicy = JSON.stringify(redrivePolicy);
                }
                this.awsSQS.createQueue(params, queueCreated);
            }

        ], callback);
    }

    deleteQueue(queueName, callback) {

        async.waterfall([

            (queueChecked) => {
                this.getQueues(queueName, (error, list) => {
                    if (error) return queueChecked(error);
                    if (!list) {
                        let newError = new Error(`Queue ${queueName} is absent. Nothing to delete`);
                        return queueChecked(newError);
                    }
                    if (list.length > 1) {
                        let newError = new Error(`There are more than one queue with name ${queueName}. Error ...`);
                        return queueChecked(newError);
                    }
                    queueChecked(null, list[0]);
                })
            },

            (queueUrl, queueDeleted) => {
                let params = {QueueUrl: queueUrl};
                this.awsSQS.deleteQueue(params, queueDeleted);
            }

        ], callback);
    }

    getQueues(queueName, callback) {
        let params = {QueueNamePrefix: queueName};
        this.awsSQS.listQueues(params, (error, data) => {
            if (error) return callback(error);
            let result = null;
            if (data && data.QueueUrls && (data.QueueUrls.length > 0)) {
                result = data.QueueUrls;
            }
            callback(null, result);
        })
    }

    publish(subject, options, {meta, payload}) {
        _.merge(options, this.options);
        let queueName = buildFullQueueName(subject, options);
        let queue = this.publishQueues.get(queueName);
        if (!queue) {
            queue = new Queue(queueName, this.awsSQS, options);
            this.publishQueues.set(queueName, queue);
            queue.initPublishQueue();
        }

        queue.retrieveQueueUrl((error, url) => {
            if (error) {
                logger.error(`Errors occurred during getting queue ${queueName}: ${error.message}`);
                process.exit(1);
                throw error;
            }

            let data = {MessageBody: JSON.stringify(payload || {}), MessageAttributes: {}};
            if (options.fifo) {
                let uniqId = generateUuid();
                data.MessageGroupId = options.fifo.messageGroupId || uniqId;
                data.MessageDeduplicationId = uniqId;
            }
            if (meta) {
                for (let key of Object.keys(meta)) {
                    let value = meta[key];
                    if (!value) continue;
                    data.MessageAttributes[key] = {
                        DataType: 'String',
                        StringValue: value
                    }
                }
            }
            queue.addMessageToSend(data);
        });
    }

    process(subject, options, handler) {
        let queueOptions = {};
        _.merge(queueOptions, this.options, options);
        let queueName = buildFullQueueName(subject, queueOptions);
        let queue = this.processQueues.get(queueName);
        if (!queue) {
            queue = new Queue(queueName, this.awsSQS, queueOptions);
            this.processQueues.set(queueName, queue);
        }
        queue.retrieveQueueUrl((error) => {
            if (error) {
                logger.error(`Errors occurred during getting queue ${queueName}: ${error.message}`);
                process.exit(1);
                throw error;
            }

            logger.info(`Service successfully subscribed to process messages from queue ${queueName}`);
            queue.initProcessQueue();
            queue.setProcessHandler(handler);
            for (let i = 0; i < queueOptions.numberOfListeners; i++) {
                queue.startProcessing();
            }
        });
    }

    request(subject, options, {meta, payload}, responseHandler) {
        if (!this.responseQueueName) {
            throw new Error('Response queue is not defined');
        }
        _.merge(options, this.options);
        if (!meta) meta = {};
        meta.responseQueueName = this.responseQueueName;
        let requestId = generateUuid();
        meta.requestId = requestId;
        this.publish(subject, options, {meta, payload});

        //WAIT FOR RESPONSE
        let timeoutId = setTimeout(() => {
            this.internalMessageBus.removeAllListeners(requestId);
            responseHandler({payload: {success: false, status: 408, errorMessage: 'request timeout'}});
        }, this.options.requestTimeout);
        this.internalMessageBus.once(requestId, (data) => {
            clearTimeout(timeoutId);
            responseHandler(data);
        })

    }

    /**
     * // TODO think about it
     * Right now if message processing finished with error (invoke done(err)), we don't send response to client
     */
    listen(subject, options, handler) {
        _.merge(options, this.options);
        this.process(subject, options, (data, done) => {
            handler(data, (error, responseData) => {
                if (error) {
                    return done(error);
                }
                done();
                let responseQueueName = data.meta.responseQueueName;
                this.publish(responseQueueName, options, responseData);
            });
        })
    }

    stop() {
        // STOP LISTEN QUEUES
        this.processQueues = null;
        if (!this.responseQueueNam) return;
        // DELETING RESPONSE QUEUE
        this.deleteQueue(buildFullQueueName(this.responseQueueName, this.options), (error) => {
            if (error) {
                logger.error(error);
            } else {
                logger.info(`Response queue ${this.responseQueueName} has been deleted`);
            }
        });

    }

    /**
     * Signals to catch docker termination signals
     */
    _addPOSIXSignalsListeners () {
        process.on('SIGINT', () => {
            logger.info('Catched SIGINT signal');
            this._cleanUpAndShutDown(1)
        });
        process.on('SIGTERM', () => {
            logger.info('Catched SIGTERM signal');
            this._cleanUpAndShutDown(0)
        });
        process.on('SIGHUP', () => {
            logger.info('Catched SIGHUP signal');
            this._cleanUpAndShutDown(1)
        });
        process.on('SIGQUIT', () => {
            logger.info('Catched SIGQUIT signal');
            this._cleanUpAndShutDown(1)
        });

        process.on('uncaughtException', function(e) {
            logger.error('Uncaught exception...');
            logger.error(e.stack);
            process.exit(1);
        });
        process.on('exit', (code) => {
            logger.info(`Exiting with code ${code}`);
        });
    }

    _cleanUpAndShutDown(code) {
        // NOTHING TO CLEAN UP
        if (!this.responseQueueName) return process.exit(code);

        // DELETING RESPONSE QUEUE
        this.deleteQueue(buildFullQueueName(this.responseQueueName, this.options), (error) => {
            if (error) {
                logger.error(error);
            } else {
                logger.info(`Response queue ${this.responseQueueName} has been deleted`);
            }
            process.exit(code)
        });
    }
}

module.exports = SQS;
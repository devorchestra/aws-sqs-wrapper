let async = require('async');
let util = require('util');

let logger = require('./logger');

class Queue {

    constructor(name, awsSQS, options) {
        this.name = name;
        this.awsSQS = awsSQS;
        this.options = options;
        this.url= undefined;
        this.messagesToDelete = [];
        this.deleteMessageTimeoutId = undefined;
        this.processHandler = undefined;
        this.messagesToSend = [];
        this.messagesToSendSize = 0;
        this.sendMessageTimeoutId = undefined;
    }

    getQueueName() { return this.name; }

    retrieveQueueUrl(callback) {
        if (this.url) return process.nextTick(() => {callback(null, this.url)});

        return this.awsSQS.getQueueUrl({QueueName: this.name}, (error, data) => {
            if (error) return callback(error);
            this.url = data.QueueUrl;
            return callback(null, this.url);
        });
    }

    initPublishQueue() {
        this.addTimeoutForMessageSending()
    }

    initProcessQueue() {
        this.addTimeoutForMessageDeleting()
    }


    setProcessHandler(handler) { this.processHandler = handler; }

    // Be aware, this.url has to be defined before invoking this method
    startProcessing() {
        // TODO implement ReceiveRequestAttemptId
        let params = {
            QueueUrl: this.url,
            WaitTimeSeconds: this.options.retrieveDuration,
            MaxNumberOfMessages: this.options.numberOfRetrieveMessages,
            MessageAttributeNames: this.options.messageAttributeNames,
            VisibilityTimeout: this.options.visibilityTimeout
        };
        this.awsSQS.receiveMessage(params, (error, data) => {
            if (error) {
                logger.error(`Errors occurred during receiving messages from ${this.name} queue:`);
                logger.error(error);
            }

            this.startProcessing();
            if (!data || !data.Messages) return;

            async.each(data.Messages, (taskMessage, messageProcessed) => {
                let data = {};
                data.payload = JSON.parse(taskMessage.Body);
                if (taskMessage.MessageAttributes) {
                    data.meta = {};
                    for (let key of Object.keys(taskMessage.MessageAttributes)) {
                        data.meta[key] = taskMessage.MessageAttributes[key].StringValue;
                    }
                }
                this.processHandler(data, (error) => {
                    if (!error) {
                        this.addMessageToDelete({ReceiptHandle: taskMessage.ReceiptHandle});
                    }
                    messageProcessed(null);
                });

            }, () => {});
        });
    }

    addMessageToDelete(message) {
        message.Id = this.messagesToDelete.length.toString();
        this.messagesToDelete.push(message);
        if (this.messagesToDelete.length < 10) return;

        this.deleteMessages();
    }

    addMessageToSend(message) {
        let messageSize = JSON.stringify(message).length;
        if (messageSize > this.options.messageSizeLimit) {
            return logger.error(`Errors occurred during sending messages to queue ${this.name}: message size limit exceeded (${messageSize}); Message Attributes: ${util.format(message.MessageAttributes)}`);
        }

        if (messageSize + this.messagesToSendSize > this.options.messageSizeLimit) {
            this.sendMessages();
            message.Id = this.messagesToSend.length.toString();
            this.messagesToSendSize += messageSize;
            return this.messagesToSend.push(message);
        }

        message.Id = this.messagesToSend.length.toString();
        this.messagesToSendSize += messageSize;
        this.messagesToSend.push(message);
        if (this.messagesToSend.length < 10) return;

        this.sendMessages();
    }

    // Be aware, this.url has to be defined before invoking this method
    // TODO RETRY deleting
    deleteMessages() {
        clearTimeout(this.deleteMessageTimeoutId);
        if (this.messagesToDelete.length === 0) return this.addTimeoutForMessageDeleting();

        let Entries = this.messagesToDelete;
        this.messagesToDelete = [];
        this.addTimeoutForMessageDeleting();
        this.awsSQS.deleteMessageBatch({QueueUrl: this.url, Entries}, (error) => {
            if (error) {
                logger.error(`Errors occurred during deleting messages from queue ${this.name}: ${error.message}`);
            } else {
                logger.debug(`Message (${Entries.length}) has been deleted from queue ${this.name}`);
            }
        })
    }

    // Be aware, this.url has to be defined before invoking this method
    // TODO RETRY sending
    sendMessages() {
        clearTimeout(this.sendMessageTimeoutId);
        if (this.messagesToSend.length === 0) return this.addTimeoutForMessageSending();

        let Entries = this.messagesToSend;
        this.messagesToSend = [];
        this.messagesToSendSize = 0;
        this.addTimeoutForMessageSending();
        this.awsSQS.sendMessageBatch({QueueUrl: this.url, Entries}, (error) => {
            if (error) {
                logger.error(`Errors occurred during sending messages to queue ${this.name}: ${error.message}`);
            } else {
                logger.debug(`Message (${Entries.length}) has been sent to queue ${this.name}`);
            }
        })
    }

    addTimeoutForMessageDeleting() {
        this.deleteMessageTimeoutId = setTimeout(() => {
            this.deleteMessages();
        }, this.options.deleteMessagesInterval);
    }

    addTimeoutForMessageSending() {
        this.sendMessageTimeoutId = setTimeout(() => {
            this.sendMessages();
        }, this.options.sendMessagesInterval);
    }
}

module.exports = Queue;
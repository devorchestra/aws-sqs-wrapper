const assert = require('assert');

function buildFullQueueName(subject, options) {
    // replace all dot symbols with dash
    subject = subject.replace(/\./g, '-');
    let {env = 'development', queuePrefix} = options;
    let fullQueueName = env + '-';
    if (queuePrefix) fullQueueName += queuePrefix + '-';
    fullQueueName += subject;
    if (options.fifo) fullQueueName += '.fifo';
    return fullQueueName;
}

function validateOptions(options) {
    assert(options.env, 'Parameter "env" should be set');
    assert(options.region, 'Parameter "region" should be set');
}


exports.buildFullQueueName = buildFullQueueName;
exports.validateOptions = validateOptions;
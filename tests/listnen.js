let SQS = require('../index');

let broker = new SQS({
    env: 'test',
    numberOfRetrieveMessages: 10,
    retrieveDuration: 20,
    region: 'eu-west-1',
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
    numberOfListeners: 2,
    visibilityTimeout: 15,
    sslEnabled: true
});

// USE WITH REQUEST METHOD

broker.listen('listener-queue', {}, ({meta, payload}, done) => {
    console.log(`Meta: ${meta}`);
    console.log(`payload: ${payload}`);
    setTimeout(() => {
        done(null, {meta, payload: {xui: 1}});
    }, Math.floor(2000 * Math.random()))

})
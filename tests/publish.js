let SQS = require('../index');

let broker = new SQS({
    env: 'test',
    numberOfRetrieveMessages: 10,
    retrieveDuration: 20,
    region: 'eu-west-1',
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
    numberOfListeners: 2,
    visibilityTimeout: 30,
    sslEnabled: true
});

// USE WITH PROCESS METHOD

broker.publish('process-queue', {}, {
    meta: {}, payload: {dima: "kostia"}
});
let SQS = require('../index');

let broker = new SQS({
    env: 'test',
    numberOfRetrieveMessages: 10,
    retrieveDuration: 20,
    region: 'eu-west-1',
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
    numberOfListeners: 2,
    visibilityTimeout: 30,
    sslEnabled: true
});

// USE WITH LISTEN METHOD

broker.initializeWithResponseQueue({responseSubject: "request-queue", redrivePolicy: {
    deadLetterTargetArn: "arn:aws:sqs:eu-west-1:381432494051:dead-test-request-queue",
    maxReceiveCount: 1
}}, (error) => {
    if (error) return console.log(error);

    broker.request('listener-queue', {}, {
        meta: {}, payload: {mama: "papa"}
    }, (data) => {
        console.log(data);
    })
});
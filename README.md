# aws-sqs-wrapper

A wrapper for npm [aws sdk module](https://www.npmjs.com/package/aws-sdk) for convenient work with AWS SQS.

Main goal of this module is to provide for developers an easy error handling function and common patterns to work with message broker:

1. publish-subscribe

2. request-response

## How to use
See index.d.ts types file

## Documentation
You can use `typedoc` to generate documentation of the module:
```
$ typedoc --out docs/ --includeDeclarations index.d.ts
```

## Tests
To perfrom tests you need to have access to the AWS SQS